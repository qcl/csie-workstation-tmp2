NTU CSIE WorkStation /tmp2 Usage
----
Show the /tmp2 usage of NTU CSIE WorkStation

Now running at [http://w.csie.org/~r01922024/tmp2](http://w.csie.org/~r01922024/tmp2)


# Script
* script
    * getTemp2Space.sh

Using ```crontab -e``` add ```*/5 * * * * sh getTemp2Space.sh```, let it update per 5 mins.

This script will put a ```{hostname}.json``` into ```{webroot}/tmp2/``` and the web interface will get it via ajax.

# Web Interface
Now running at [http://w.csie.org/~r01922024/tmp2](http://w.csie.org/~r01922024/tmp2)

* web
    * js - javascript files
    * css - css files
    * fonts - for bootstrap

