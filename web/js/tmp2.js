$(function(){

var $table = $("#usageTable");
var $tbody = $table.find("tbody");
var $time = $("#time");

for(var i=1;i<=20;i++){
    if(i==15)continue;
    var hostname = "linux"+i;
    var dom = $("<tr id=\""+hostname+"\">").append("<td id=\"name\">"+hostname+"</td>").append("<td id=\"used\">").append("<td id=\"rest\"").append("<td id=\"total\">").append("<td id=\"percent\">");

    $tbody.append(dom); 

    $.ajax({
        url:hostname+".json",
        success: function(result){
            var dom = $("#"+result.machine);
            var percent = parseInt(result.percent.split("%")[0]);
            dom.find("#used").html(result.used);
            dom.find("#rest").html(result.rest);
            dom.find("#total").html(result.total);
            dom.find("#percent").html(result.percent);

            if(percent >= 90){
                dom.addClass("danger");    
            }else if(percent >= 50){
                dom.addClass("warning");
            }else{
                dom.addClass("success");
            }

            var updateTime = new Date(result.time*1000);
            var timeStr = updateTime.toDateString() + " " + updateTime.toTimeString();
            $time.html(timeStr);

        },
        error: function(){
        },
        dataType: "json"
    });

}
});
